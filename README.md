# TORASPACE

[![pipeline status](https://gitlab.com/manut.de/toraspace/badges/master/pipeline.svg)](https://gitlab.com/manut.de/toraspace/-/commits/master)

TORASPACE is a aarch64 Docker container to run the ninvaders game in a
minimal Debian based environment.

To play the game, use the following command:

```bash
pi@raspberrypi:~ $ docker run -ti registry.gitlab.com/manut.de/toraspace
Unable to find image 'registry.gitlab.com/manut.de/toraspace:latest' locally
latest: Pulling from manut.de/toraspace
4e6164a63b7b: Pull complete
7b1eba202f11: Pull complete
Digest: sha256:4d6b9ec28e62e71227c02b3cd713100b15d8b13fd0161a4e7aee0f2fa6992b66
Status: Downloaded newer image for registry.gitlab.com/manut.de/toraspace:latest
```
