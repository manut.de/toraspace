# SPDX-License-Identifier: GPL-2.0
#
# Copyright 2020 - Manuel Traut <manut@mecka.net>

FROM debian:buster-slim

RUN apt update && \
    apt install -y ninvaders && \
    rm -rf /var/lib/apt/lists/*

CMD /usr/games/ninvaders
